<?php
namespace Api\Controller;
use Think\Controller;
class AppsController extends CommonController {
    public function index(){
        $Apps = D('Apps');
        $map['apid'] = $this->apid;
        $data = $Apps->where($map)->find();
        $this->ajaxReturn($data,$this->format);
    }
}