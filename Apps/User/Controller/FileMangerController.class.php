<?php
namespace User\Controller;
use Think\Controller;
class FileMangerController extends CommonController {

    public function updateField($value='',$pk = '',$name = '')
    {
        if (!$name || !$pk  ) return;
        if ($name == 'icon') {
            $appath = 'User/'.numberDir($this->apid).'icon/content/';
            $appicon = $appath.$pk.'.png';
            if (!is_dir($appath)) {
                mkdir($appath,0777,true);
            }
            @copy($value, $appicon);
            $value = $appicon;
        }

        if ($name == 'picurl') {
            $appath = 'User/'.numberDir($this->apid).'appbg/content/';
            $appicon = $appath.$pk.'.png';
            if (!is_dir($appath)) {
                mkdir($appath,0777,true);
            }
            @copy($value, $appicon);
            $value = $appicon;
        }


        $model = M( $this->con );
        $map['id'] = $pk;
        $map['mid'] = $this->mid;
        $data[$name] = $value;
        $model->where($map)->save($data);

        $this->success( '成功!');
    }

    public function markerAppicon($bg = '',$txt = '')
    {
        $bg = urldecode($bg);
        $appath = 'User/'.numberDir($this->apid).'icon/';
        $appicon = $appath.'icon.png';
        if (!is_dir($appath)) {
            mkdir($appath,0777,true);
        }
        $ret = copy($bg, $appicon);
        $WaterMask = new \Common\ORG\WaterMask($appicon);
        $WaterMask->waterStr = $txt;
        $WaterMask->pos = 5;
        $WaterMask->fontSize = 60;
        $WaterMask->output(); 

        $map['mid'] = $this->mid;
        $map['id'] = $this->apid;
        $Apps = M('Apps');
        $data['icon'] = $appicon;
        $Apps->where($map)->save($data);
    }

    public function getlist()
    {
        $data['code'] = 0;
        $data['message'] = 'Common:OK';
        $m = D('Attr');
        $map['mid'] = $this->mid;
        $ret = $m->getPage($map,$curPage,120);
        $d = array();
        foreach ($ret['volist'] as $k) {
            $vo = array();
            $vo['asset_id'] = $k['id'];
            $vo['owner_id'] = $k['mid'];
            $vo['asset_type'] = 0;
            $vo['host_context'] = 0;
            $vo['hash_value'] = md5($k['id']);
            $vo['target_uri'] = $k['url'];
            $vo['display_name'] = $k['name'];
            $vo['file_size'] = $k['size'];
            // $vo['file_size'] = $k['size'];
            $d[] = $vo;
        }
        $data['data'] = $d;
        $this->ajaxReturn($data);
    }

    public function index($curPage =1,$pageSize = 10){
        
        if (IS_POST) {
            $m = D('Attr');
            if ( method_exists( $this, '_filter' ) ) {
                $map = $this->_filter();
            }
            $map['mid'] = $this->mid;
            $ret = $m->getPage($map,$curPage,(int)$pageSize);
            $dd = array();
            foreach ($ret['volist'] as $k) {
                $k['ctime'] = date('Y-m-d',$k['ctime']);
                if ($k['dirname'] == 'cover') 
                    $k['dirname'] = '封面';
                elseif($k['dirname'] == 'icon')
                    $k['dirname'] = '图标';
                elseif($k['dirname'] == 'background')
                    $k['dirname'] = '背景';
                $dd[] = $k;
            }
            $data['success'] = true;
            $data['data'] = $dd;
            $data['totalRows'] = $ret['count'];
            $data['curPage'] = $curPage;
            $this->ajaxReturn($data);
        }
        $this->display();
    }


    public function upload($dirname = '')
    {
        $upload = new \Think\Upload();

        $upload->subName   =     array('date','Ymd');
        $upload->autoSub   =     true ;
        $upload->maxSize   =     3145728 ;
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');
        $upload->rootPath  =     './Uploads/'; // 设置附件上传根目录
        $upload->savePath  =     $this->mid.'/'.$dirname.'/'; // 设置附件上传（子）目录
        $info   =   $upload->upload();
        $Attr = M('Attr');
        if (!$dirname) {
            $this->error('请选择保存文件夹');
        }
        if(!$info) {
            $this->error($upload->getError());
        }else{
            $u = $_SERVER['HTTP_REFERER'];
            $u_ar = explode("index.php", $u);
            $url_path = $u_ar[0];
            foreach($info as $f){
                $fp = 'Uploads/'.$f['savepath'].$f['savename'];
                $data['mid'] = $this->mid;
                $data['size'] = $f['size'];
                $data['type'] = $f['type'];
                $data['fpath'] = $fp;
                $data['dirname'] = $dirname;
                $data['originalname'] = $f['name'];
                $data['suffix'] = $f['ext'];
                $data['url'] = $url_path.$fp;
                $data['ctime'] = time();
                $data['filename'] = $f['savename'];
                $data['dirpath'] = 'Uploads/'.$dirname.'/'.$f['savepath'];
                $pk = $Attr->add($data);
            }
            $this->ajaxReturn('上传保存成功');
        }
    }


    public function file($dirname = 'cover')
    {
        $upload = new \Think\Upload();

        $upload->subName   =     array('date','Ymd');
        $upload->autoSub   =     true ;
        $upload->maxSize   =     3145728 ;
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');
        $upload->rootPath  =     './Uploads/'; // 设置附件上传根目录
        $upload->savePath  =     $this->mid.'/'.$dirname.'/'; // 设置附件上传（子）目录
        $info   =   $upload->upload();
        $Attr = M('Attr');
        
        if(!$info) {
            $this->error($upload->getError());
        }else{
            $u = $_SERVER['HTTP_REFERER'];
            $u_ar = explode("index.php", $u);
            $url_path = $u_ar[0];
            $dd = array();
            foreach($info as $f){
                $fp = 'Uploads/'.$f['savepath'].$f['savename'];
                $data['mid'] = $this->mid;
                $data['size'] = $f['size'];
                $data['type'] = $f['type'];
                $data['fpath'] = $fp;
                $data['dirname'] = $dirname;
                $data['originalname'] = $f['name'];
                $data['suffix'] = $f['ext'];
                $data['url'] = $url_path.$fp;
                $data['ctime'] = time();
                $data['filename'] = $f['savename'];
                $data['dirpath'] = 'Uploads/'.$dirname.'/'.$f['savepath'];
                $pk = $Attr->add($data);

                $vo =  array();
                $vo['asset_id'] = $pk;
                $vo['owner_id'] = $this->mid;
                $vo['asset_type'] = 0;
                $vo['host_context'] = 0;
                $vo['hash_value'] = md5($pk);
                $vo['target_uri'] = $data['url'];
                $vo['display_name'] = $f['name'];
                $vo['file_size'] = $f['size'];
                // $vo['file_size'] = $k['size'];
                $dd[] = $vo;
            }
            $ret['code'] = 0;
            $ret['message'] = 'Common:OK';
            $ret['data'] = $dd;
            $this->ajaxReturn($ret);
        }
    }

    public function delete($id='')
    {
        $Attr = M('Attr');
        $map['mid'] = $this->mid;
        $map['id'] = $id;
        $v = $Attr->where($map)->find();
        $ret = $Attr->where($map)->delete();
        if ($ret !== false) {
            unlink($v['fpath']);
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    public function deletPath($fpath ='')
    {
        $Attr = M('Attr');
        $map['mid'] = $this->mid;
        $map['fpath'] = $fpath;
        $v = $UserAttr->where($map)->find();
        $ret = $UserAttr->where($map)->delete();
        if ($ret !== false) {
            unlink($v['fpath']);
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }
}